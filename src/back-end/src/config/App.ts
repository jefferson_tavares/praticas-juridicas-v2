import { AppAuthMiddleware } from './../middleware/AppAuthMiddleware';
import { AppEnv } from './AppEnv';
import { AppRouter } from './AppRouter';
import Express from 'express';
import Cors from 'cors';
import DotEnv from 'dotenv';
import BodyParser from 'body-parser';

export class App {
    private express = Express();
    private router = new AppRouter(this.express);

    public run() {
        this.configEnv();

        this.configBodyParsers()
        this.configMiddlewares();
        this.configCORS();
        this.configRoutes();

        this.listen();
    }

    private configEnv() {
        AppEnv.config();
    }

    private configBodyParsers() {
        this.express.use(BodyParser.urlencoded({extended: true}));
        this.express.use(BodyParser.json());
    }

    private configMiddlewares() {
        AppAuthMiddleware.config();
    }

    private configCORS() {
        this.express.use(Cors());
    }

    private configRoutes() {
        this.router.configRoutes();
    }

    private listen() {
        this.express.listen(AppEnv.getEnv().http.port, () => {
            console.log('Listening on: ' + AppEnv.getEnv().http.port);
        });
    }
}