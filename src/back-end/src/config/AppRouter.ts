import { ClienteRouter } from './router/ClienteRouter';
import { ConfigurableRouter } from './router/ConfigurableRouter';
import { Router, Application } from 'express';

export class AppRouter {
    constructor(private express: Application) {}
    public configRoutes() {
        this.getConfigurableRouters().forEach(routerConfig => {
            this.express.use(routerConfig.config());
        });
    }

    private getConfigurableRouters(): ConfigurableRouter[] {
        return [
            new ClienteRouter()
        ];
    }
}