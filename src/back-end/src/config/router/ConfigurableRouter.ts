import { Router } from "express";

export interface ConfigurableRouter {
    config(): Router;
}