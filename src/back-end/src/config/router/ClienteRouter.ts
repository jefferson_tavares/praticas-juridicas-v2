import { AppAuthMiddleware } from './../../middleware/AppAuthMiddleware';
import Passport from 'passport';
import { ClienteRequestHandler } from './../../request-handler/ClienteRequestHandler';
import Express, { Router, Request, Response } from 'express';
import { ConfigurableRouter } from './ConfigurableRouter';

export class ClienteRouter implements ConfigurableRouter {
    config(): Router {
        const router = Express.Router();
        const requestHandler = new ClienteRequestHandler();

        const basePath = '/api/cliente';

        router.post(`${basePath}/:id?`, AppAuthMiddleware.basic(), requestHandler.salvar);
        router.get(`${basePath}/:id?`, AppAuthMiddleware.basic(), requestHandler.listar);
        router.delete(`${basePath}/:id?`, AppAuthMiddleware.basic(), requestHandler.deletar);
        router.post(`${basePath}/:id?`, AppAuthMiddleware.basic(), requestHandler.desfazerDeletar);

        return router;
    }
}