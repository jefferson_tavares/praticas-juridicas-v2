import { AppEnv } from './AppEnv';
import Mysql from 'mysql';

export class AppDatabase {
    private static getConnection() {
        const databaseEnv = AppEnv.getEnv().database;

        return Mysql.createConnection({
            host: databaseEnv.host,
            user: databaseEnv.user,
            password: databaseEnv.password,
            database: databaseEnv.name            
        });
    }

    static query(sql: string, params: any[]): Promise<any> {
        return new Promise((resolve, reject) => {
            const connection = this.getConnection();

            connection.connect(error => {
                if(error) {
                    return reject(error);
                }

                connection.query(sql, params, (error, result) => {
                    if(error) {
                        return reject(error);
                    }

                    resolve(result);
                });
            });
        });
    }
}