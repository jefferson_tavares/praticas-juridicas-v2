import DotEnv from 'dotenv';
import Path from 'path';

export class AppEnv {
    static getEnv() {
        return {
            database: {
                host: process.env.DATABASE_HOST,
                user: process.env.DATABASE_USER,
                password: process.env.DATABASE_PASSWORD,
                name: process.env.DATABASE_NAME
            },
        
            http: {
                port: process.env.HTTP_PORT
            }
        };
    }

    static config() {
        const path = Path.join(__dirname, '..', '..', '.env');

        console.log(path);

        DotEnv.config({
            path: path
        });
    }
}