import { PessoaRepository } from './../repository/PessoaRepository';
import { PessoaRuler } from './../ruler/PessoaRuler';
import { AppEnv } from './../config/AppEnv';
import { Request, Response } from 'express';
import Lodash from 'lodash';

export class ClienteRequestHandler {
    salvar(request: Request, response: Response) {
        console.log('Salvar: ', JSON.stringify(request.body));

       const ruler = new PessoaRuler();
       
       const result = ruler.isValid(request.body);
       const idEmpresa = request.headers.idempresa;

       if(result.isOK() && idEmpresa) {
           PessoaRepository.salvar(Number(idEmpresa), request.body)
            .then((result: boolean) => {
                if(result) {
                    return response.json({result: true});
                }

                return response.json({result: false});
            })
            .catch(error => {
                return response.json(error);
            });
       }else {
           return response.json(result.errors());
       }
    }

    listar(request: Request, response: Response) {
        console.log('Listar endpoint');
        response.send('There you go :)');
    }

    deletar(request: Request, response: Response) {

    }

    desfazerDeletar(request: Request, response: Response) {

    }
}