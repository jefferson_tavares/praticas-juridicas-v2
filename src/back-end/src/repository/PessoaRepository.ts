import { AppDatabase } from './../config/AppDatabase';
import { Pessoa } from 'common';

export class PessoaRepository {
    static salvar(idEmpresa: number, pessoa: Pessoa) {
        const query = 'INSERT INTO pessoa (nome, cpf, cnpj, idEmpresa, conjuge, responsavelLegal, dataNascimento, cadastradaEm) VALUES (?, ?, ?, ?, ?, ?, ?, now())';

        return AppDatabase.query(query, [
            pessoa.nome,
            pessoa.cpf,
            pessoa.cnpj,
            idEmpresa,
            pessoa.conjuge,
            pessoa.responsavelLegal,
            pessoa.dataNascimento
        ]).then(result => {
            console.log(result);
            
            return result.affectedRows === 1;
        });
    }
}