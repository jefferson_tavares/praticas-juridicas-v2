import { AppDatabase } from './../config/AppDatabase';

export class UsuarioRepository {
    static autenticar(login: string, senha: string): Promise<boolean> {
        console.log('Autenticar');

        const sql = 'SELECT * FROM usuario WHERE login = ?';

        return AppDatabase.query(sql, [login])
            .then((results: ReadonlyArray<any>) => {
                if (results.length === 0) {
                    return false;
                }

                if (results.length === 0) {
                    return false;
                }

                const user = results[0];

                if (user.senha !== senha) { //TODO adicionar criptografia
                    return false;
                }

                return true;
            });
    }

    static novoAcesso(login: string): Promise<boolean> {
        const sql = 'UPDATE usuario SET ultimoAcesso = NOW() WHERE login = ?';

        return AppDatabase.query(sql, [login])
            .then(result => {
                return result.changedRows === 1;
            });
    }
}
