import Passport from 'passport';
import PassportLocal, { BasicStrategy } from 'passport-http';
import { Application } from 'express';
import { UsuarioRepository } from '../repository/UsuarioRepository';

export class AppAuthMiddleware {
    public static config() {
        const httpBasicStrategy = new BasicStrategy((username, password, callback) => {
            return UsuarioRepository.autenticar(username, password)
                .then(result => {
                    if(!result) {
                        return Promise.resolve(false);
                    }

                    return UsuarioRepository.novoAcesso(username);
                })
                .then(result =>  callback(null, result)) 
                .catch(error => callback(error, null));
        });

        Passport.use(httpBasicStrategy);
    }

    public static basic() {
        return Passport.authenticate('basic', {
            session: false,
        });
    }
}