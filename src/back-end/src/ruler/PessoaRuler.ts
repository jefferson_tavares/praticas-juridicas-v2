import { FieldRule } from './PessoaRuler';
import { Pessoa } from 'common';

export interface FieldRule {
    field: string;
    mensagem: string;
    isOK: boolean;
}

export interface RulerResult {
    validatedFields: ReadonlyArray<FieldRule>;

    errors: () => ReadonlyArray<FieldRule>;

    isOK: () => boolean;
}

export class BasicRulerResult implements RulerResult {
    constructor(public validatedFields: ReadonlyArray<FieldRule>) { }

    isOK(): boolean {
        return this.validatedFields.reduce((currentValue, field) => {
            return currentValue && field.isOK;
        }, true)
    }

    errors() : ReadonlyArray<FieldRule> {
        return this.validatedFields.filter(field => {
            return !field.isOK;
        })
    }
}

export class PessoaRuler {
    isValid(pessoa: Pessoa): RulerResult {
        return new BasicRulerResult([
            ...this.isDocumentacaoBasicaPreenchida(pessoa.cpf, pessoa.cnpj)
        ]);
    }

    private isDocumentacaoBasicaPreenchida(
        cpf: string | undefined,
        cnpj: string | undefined
    ): FieldRule[] {
        return [
            {
                field: 'cpf|cnpj',
                mensagem: 'CPF ou CNPJ deverão ser preenchidos',
                isOK: cpf !== undefined || cnpj !== undefined
            }
        ];
    }
}