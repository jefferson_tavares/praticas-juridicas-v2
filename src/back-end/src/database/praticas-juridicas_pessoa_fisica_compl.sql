CREATE DATABASE  IF NOT EXISTS `praticas-juridicas` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `praticas-juridicas`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: praticas-juridicas
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pessoa_fisica_compl`
--

DROP TABLE IF EXISTS `pessoa_fisica_compl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoa_fisica_compl` (
  `idPessoa` int(11) NOT NULL,
  `dataNascimento` date DEFAULT NULL,
  `cpf` varchar(12) DEFAULT NULL,
  `rg` varchar(9) DEFAULT NULL,
  `conjuge` int(11) DEFAULT NULL,
  `reponsavelLegal` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPessoa`),
  KEY `fk_id_pessoa_conjuge_idx` (`conjuge`),
  KEY `fk_id_pessoa_responsavel_legal_idx` (`reponsavelLegal`),
  CONSTRAINT `fk_id_pessoa` FOREIGN KEY (`idPessoa`) REFERENCES `pessoa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_pessoa_conjuge` FOREIGN KEY (`conjuge`) REFERENCES `pessoa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_pessoa_responsavel_legal` FOREIGN KEY (`reponsavelLegal`) REFERENCES `pessoa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoa_fisica_compl`
--

LOCK TABLES `pessoa_fisica_compl` WRITE;
/*!40000 ALTER TABLE `pessoa_fisica_compl` DISABLE KEYS */;
INSERT INTO `pessoa_fisica_compl` VALUES (1,'1995-06-30','61651722021',NULL,NULL,NULL);
/*!40000 ALTER TABLE `pessoa_fisica_compl` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-26  9:29:50
