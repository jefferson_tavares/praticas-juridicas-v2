export interface Pessoa {
    id: number;
    nome: string;
    cpf?: string;
    cnpj?: string;
    idEmpresa: number;
    conjuge?: number;
    responsavelLegal?: number;
    dataNascimento: Date;
    cadastradaEm: Date;
    ultimaAtualizacao: Date;
}